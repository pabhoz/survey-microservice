<?php

/**
 * Description of Model
 *
 * @author pabhoz
 * @coauthors Sebas7103 - CamiloPochi
 */
class Model {
    
    private static $db;
    protected static $table;

    private static function getConnection(){
     if(!isset(self::$db)){
        self::$db = new Database(_DB_TYPE,_DB_HOST,_DB_NAME,_DB_USER,_DB_PASS);
     }
    }
    
    public function getRelationship($t){
        self::getConnection();
        return self::$db->getRelationship($t);
    }


    public static function setTable($table){
        self::$table = $table;
    }
            
    public static function getAll(){
        self::getConnection();
        $sql = "SELECT * FROM ".static::$table.";";
        return $results = self::$db->select($sql);
    }

    public static function where($field, $value){
        self::getConnection();
        $sql = "SELECT * FROM ".static::$table." WHERE ".$field." = :".$field;
        $results = self::$db->select($sql, array(":".$field=>$value) );
        return $results;
    }

    public static function whereR($attr,$field, $value, $tableR){
            self::getConnection();
            $sql = "SELECT ".$attr." FROM ".$tableR." WHERE ".$field." = :".$field;
            $results = self::$db->select($sql, array(":".$field=>$value) );

            return $results;
    }

    public static function whereN($attr,$field, $tableR){
            self::getConnection();
            $sql = "SELECT ".$attr." FROM ".$tableR." WHERE ".$field. " IS NULL";
            $results = self::$db->select($sql );

            return $results;
    }

    public function create(){
        self::getConnection();

        $values = $this->getMyVars($this);
        $has_many = self::checkRelationship("has_many",$values);
        //$has_one = self::checkRelationship("has_one",$values);
        $result = self::$db->insert(static::$table,$values);
        
        if($result){
            $result = array('error'=>0,'getID'=> self::$db->lastInsertId(),'message'=>'Objeto Creado');
            $this->setId( $result["getID"] ) ;
        }else{
            $result = array('error'=>1,'getID'=> null,'message'=> ''/*self::$db->getError()**/);
        }
        if($has_many){ 
            $rStatus = self::saveRelationships($has_many); 
            if($rStatus["error"]){
                //Logger::alert("Error saving relationships",$rStatus["trace"],"create");
            }
        }
        return $result;
    }

    public function update(){
        self::getConnection();
            
        $values = $this->getMyVars($this);
        $has_many = self::checkRelationship("has_many",$values);
        //$has_mone = self::checkRelationship("has_one",$values);
    
        $result = self::$db->update(static::$table,$values,"id = ".$values["id"]);

        if($result){
            $result = array('error'=>0,'message'=>'Objeto Actualizado');
        }else{
            $result = array('error'=>1,'message'=> ''/*self::$db->getError()**/);
        }
        if($has_many){
            $rStatus = self::saveRelationships($has_many); 
            if($rStatus["error"]){
                //Logger::alert("Error saving relationships",$rStatus["trace"],"save");
            }
        }
        //Logger::debug("result",$result,"save");
        return $result;
    }

    public function saveRelationships($relationships){
            $log = array("error"=>0,"trace"=>array());
            foreach ($relationships as $name => $rules) {
                if(isset($rules['relationships'])){
                    foreach ($rules['relationships'] as $key => $relacion) {
                        $table = $rules["join_table"];
                                        $result = self::$db->insert($table,$relacion);
                                        //if(!$result){
                                          //  $log["error"] = 1;
                                           // $log["trace"][] = array('relationship'=> $name,'message'=> ''/*self::$db->getError()**/);
                                        //}*/
                    }
                }
            }
            return $log;
    }

    public function has_many($rName,$obj){
         $has_many = $this->getHas_many();
            if($has_many[$rName] != null){
                $rule = $has_many[$rName];
                if(get_class($obj) == $rule["class"]){
                    if( $this->getId() != null && $obj->getId() != null ){
                        $rule['relationships'][]= array(
                          $rule['join_self_as']=>$this->getId(),
                          $rule['join_other_as']=>$obj->getId()
                        );
                        $has_many[$rName] = $rule;
                        $this->setHas_many($has_many);
                    }else{
                        print("Se requieren llaves primarias para la relación");
                    }
                }else{
                    print("No se cumple con el tipo de objeto");
                }
            }else{
                print("No existe este tipo de relación");
            }      
        }

    public function has_one($rName,$obj){
        if( isset($this->has_one[$rName]) ){

            $rule = $this->has_one[$rName];
            if(get_class($obj) == $rule["class"]){
               $this->{$rule["join_as"]} = $obj->{$rule["join_with"]};
            }else{
                print("No se cumple con el tipo de objeto");
            }

        }else{
            print("No existe este tipo de relación");
        }  
    }

    public function set($attr,$value){
        $this->{$attr} = $value;
    }

    public function checkRelationship($rType,&$data){
            if( isset($data[$rType]) ){
                $relationship = $data[$rType];
                unset($data[$rType]);
                return $relationship;
            }else{
                return false;
            }
        }
        
    public function delete(){
            self::getConnection();
            $result = self::$db->delete(static::$table,"id = ".$this->getId());
            
            if($result){
                    $result = array('error'=>0,'message'=>'Objeto Eliminado');
            }else{
                    $result = array('error'=>1,'message'=> self::$db->errorInfo());
            }
            return $result;
    }
    
    public static function getById($id){
            return $data = array_shift(self::where("id",$id)); 
    }
    
    public function getAttrTable($table){
            self::getConnection();
            $attr = self::$db->getAttr($table);
            return $attr;
    }
    
    public function toArray(){
        $arr = [];
        foreach ($this->getMyVars() as $key => $value) {
            $arr[$key] = $this->{"get".$key}();
        }
        return $arr;
    }
}
