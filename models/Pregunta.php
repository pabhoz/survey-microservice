<?php
/**
 * Description of Pregunta
 *
 * @author pabhoz
 */
class Pregunta extends Model{
    private $id;
    private $pregunta;
    private $id_encuesta;
    protected static $table = "Preguntas";
    
    function __construct($id, $pregunta, $id_encuesta) {
        $this->id = $id;
        $this->pregunta = $pregunta;
        $this->id_encuesta = $id_encuesta;
    }
    
    function getId() {
        return $this->id;
    }

    function getPregunta() {
        return $this->pregunta;
    }

    function getId_encuesta() {
        return $this->id_encuesta;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPregunta($pregunta) {
        $this->pregunta = $pregunta;
    }

    function setId_encuesta($id_encuesta) {
        $this->id_encuesta = $id_encuesta;
    }

    public static function getById($id) {
        $data = parent::getById($id);
        $result = new self($data["id"],$data["pregunta"],$data["id_encuesta"]);
        return $result;
    }
    
    function getMyVars(){
        return get_object_vars($this);
    }

}
