<?php
/**
 * Description of Encuesta
 *
 * @author pabhoz
 */
class Encuesta extends Model{
    private $id;
    private $titulo;
    protected static $table = "Encuesta";
  
    function __construct($id, $titulo) {
        $this->id = $id;
        $this->titulo = $titulo;
    }

    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public static function getById($id) {
        $data = parent::getById($id);
        $result = new self($data["id"],$data["titulo"]);
        return $result;
    }
    
    function getMyVars(){
        return get_object_vars($this);
    }


}
