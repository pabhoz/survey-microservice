<?php

class Index_controller extends Controller {

    function __construct() {
        parent::__construct();
    }
        
        public function getIndex($id = null){
            
            if(!is_null($id)){
             Penelope::printJSON(Encuesta::getById($id)->toArray()); 
            }else{
             Penelope::printJSON(Encuesta::getAll());   
            }
            
        }
        
        public function getPregunta($id = null){
            
            if(!is_null($id)){
             Penelope::printJSON(Pregunta::getById($id)->toArray()); 
            }else{
             Penelope::printJSON(Pregunta::getAll());   
            }
            
        }
        
        public function postIndex(){
            $keys = ["id","titulo"];
            $this->validateKeys($keys, $_POST);
                 
            $srv = new Encuesta($_POST["id"], $_POST["titulo"]);
            $srv->create();
            
            $response = [
               "status"=>200,
                "msg"=>"Survey created",
                "survey"=>  Penelope::arrayToJSON($srv->toArray())
            ];
            Penelope::printJSON($response);
        }
        
        public function postPregunta(){
            $keys = ["id","pregunta","id_encuesta"];
            $this->validateKeys($keys, $_POST);
                 
            $qstn = new Pregunta($_POST["id"], $_POST["pregunta"],$_POST["id_encuesta"]);
            $qstn->create();
            
            $response = [
               "status"=>200,
                "msg"=>"Question created",
                "question"=>  Penelope::arrayToJSON($qstn->toArray())
            ];
            Penelope::printJSON($response);
        }
        
        public function putIndex(){
            $_PUT = $this->_PUT;
            $srv = Encuesta::getById($_PUT["id"]);
            foreach ($_PUT as $key => $value) {
                
                    $srv->{"set".ucfirst($key)}($value);
                
            }
            $srv->update();
            $response = [
               "status"=>200,
                "msg"=>"Survey updated",
                "survey"=>  Penelope::arrayToJSON($srv->toArray())
            ];
            Penelope::printJSON($response);
        }
        
        public function putPregunta(){
            $_PUT = $this->_PUT;
            $qstn = Pregunta::getById($_PUT["id"]);
            foreach ($_PUT as $key => $value) {
                
                    $qstn->{"set".ucfirst($key)}($value);
                
            }
            $qstn->update();
            $response = [
               "status"=>200,
                "msg"=>"Question updated",
                "question"=>  Penelope::arrayToJSON($qstn->toArray())
            ];
            Penelope::printJSON($response);
        }
        
        public function deleteIndex(){
            $_PUT = $this->_PUT;
            $srv = Encuesta::getById("id");
            if($srv->getId() != NULL){
                $result = $srv->delete();
                if($result["error"]==1){
                    $response = [
                        "status"=>200,
                         "msg"=>"Imposible to delete",
                         "error"=>$result["message"][2]
                     ];
                }else{
                    $response = [
                        "status"=>200,
                         "msg"=>"Survey deleted",
                         "survey"=>  Penelope::arrayToJSON($srv->toArray())
                     ];
                }
            }else{
                $response = [
                        "status"=>200,
                         "msg"=>"Survey not found"
                     ];
            }
            Penelope::printJSON($response);
        }
        
        public function deletePregunta($id){
            $qstn = Pregunta::getById($id);
            if($qstn->getId() != NULL){
                $result = $qstn->delete();
                if($result["error"]==1){
                    $response = [
                        "status"=>200,
                         "msg"=>"Imposible to delete",
                         "error"=>$result["message"][2]
                     ];
                }else{
                    $response = [
                        "status"=>200,
                         "msg"=>"Question deleted",
                         "question"=>  Penelope::arrayToJSON($qstn->toArray())
                     ];
                }
            }else{
                $response = [
                        "status"=>200,
                         "msg"=>"Question not found"
                     ];
            }
            Penelope::printJSON($response);
        }
}
